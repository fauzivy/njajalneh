@extends('layouts.master')
@section('head')
<link rel="stylesheet" href="../plugins/bower_components/dropify/dist/css/dropify.min.css">
@stop
@section('content')
    
<div class="container-fluid">
    <div class="col-md-12 white-box">
    <div class="flot-chart">
        <div class="flot-chart-content" id="flot-pie-chart" style="padding: 0px; position: relative;">
            <canvas class="flot-base" width="555" height="400" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 555px; height: 400px;">
            </canvas>
            <canvas class="flot-overlay" width="555" height="400" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 555px; height: 400px;">
            </canvas>
            <div class="legend"><div style="position: absolute; width: 57px; height: 64px; top: 5px; right: 5px; background-color: rgb(255, 255, 255); opacity: 0.85;"> 
            </div>
            <table style="position:absolute;top:5px;right:5px;;font-size:smaller;color:#545454">
                <tbody>
                <tr>
                    <td class="legendColorBox">
                        <div style="border:1px solid #ccc;padding:1px">
                        <div style="width:4px;height:0;border:5px solid #4f5467;overflow:hidden">
                        </div></div>
                        </td>
                    <td class="legendLabel">Series 0</td>
                </tr>
                <tr>
                    <td class="legendColorBox">
                        <div style="border:1px solid #ccc;padding:1px">
                        <div style="width:4px;height:0;border:5px solid #00c292;overflow:hidden">
                        </div></div>
                    </td>
                    <td class="legendLabel">Series 1</td>
                    </tr>
                <tr>
                <td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #01c0c8;overflow:hidden"></div></div></td><td class="legendLabel">Series 2</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid #fb9678;overflow:hidden"></div></div></td><td class="legendLabel">Series 3</td></tr></tbody></table></div></div>
        
        </div>
        <div class="col-sm-6 ol-md-6 col-xs-12">
            <div class="white-box">
                <h3 class="box-title">File Upload1</h3>
                <label for="input-file-now">Your so fresh input file — Default version</label>
                <input type="file" id="input-file-now" name="file_url" class="dropify" /> 
            </div>
        </div>
        
    </div>
    
<//div>
@stop
@section('plugins')

<script src="../../js/flot-data.js"></script>
 <script src="../plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
 <script>   
 $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
    <script src="../../plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="../../plugins/bower_components/flot/jquery.flot.pie.js"></script>
@stop