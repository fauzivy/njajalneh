<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeritasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('berita', function ($collection) {
            $collection->string('judul');
            $collection->string('media');
            $collection->string('nama_media');
            $collection->json('kategori');
            $collection->string('kata_kunci');
            $collection->string('tanggal_terbit');
            $collection->string('bahasa');
            $collection->binData('file_url');
            $collection->string('deskripsi');
            $collection->string('konten');
            $collection->string('provinsi');
            $collection->string('kabupaten');
            $collection->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('berita');
    }
}
