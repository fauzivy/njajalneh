<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Berita;
use App\Provinsi;
use Illuminate\Http\Request;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $beritas = Berita::all();
        
        return view('kelola', compact('beritas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = DB::connection('mongodb')->table('provinces')->get();
        //$regs = DB::collection('regencies')->all();
        $provinc = Provinsi::all();
        $regencies = DB::connection('mongodb')->collection('regencies')->get();
        return view('unggah', compact('provinces', 'regencies'));
        //return view('unggah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $berita = new Berita();
        $request->validate([
            'judul' => 'required:max:255',
            'fileurl' => 'required',
            'nama_media' => 'required|numeric'
        ]);
                    
        $berita = new Berita($request->all());
        
        
        // $berita->judul = $request->get('judul');
        // $berita->media = $request->get('media');
        // $berita->nama_media = $request->get('nama_media');
        // $berita->kategori = $request->get('kategori');
        // $berita->kata_kunci = $request->get('kata_kunci');
        // $berita->tanggal_terbit = $request->get('tanggal_terbit');
        // $berita->bahasa = $request->get('bahasa');
        // $berita->file_url = $request->get('file_url');
        // $berita->deskripsi = $request->get('deskripsi');
        // $berita->konten = $request->get('konten');
        // $berita->provinsi = $request->get('provinsi');
        // $berita->kabupaten = $request->get('kabupaten');
        $berita->status = 'draft';

        // for($i = 0; $i < count($content); $i++)
        // {
        //     $berita->$content[$i] = $request->get("'".$content[$i]."'");
        // }
        $berita->save();
        return redirect('kelola');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
        return view('show');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function edit($_id)
    {

        $berita = Berita::findOrFail($_id);
        return view('edit',compact('berita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $_id)
    {
        //
        $berita = Berita::findOrFail($_id);
        $berita->media = $request->get('media');
        $berita->nama_media = $request->get('nama_media');
        $berita->kategori = $request->get('kategori');
        $berita->kata_kunci = $request->get('kata_kunci');
        $berita->tanggal_terbit = $request->get('tanggal_terbit');
        $berita->bahasa = $request->get('bahasa');
        $berita->file_url = $request->get('fileurl');
        $berita->deskripsi = $request->get('deskripsi');
        $berita->konten = $request->get('konten');
        $berita->provinsi = $request->get('provinsi');
        $berita->kabupaten = $request->get('kabupaten');
        $berita->save();    
        if($berita){
            //dd($berita);
            return back();
        }else{return back();}
        
    }

    public function upload(Request $request, $_id){
        $berita = Berita::findOrFail($_id);
        $berita->status = 'diajukan';
        $berita->save();
        if($berita){
            return redirect('kelola');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Berita  $berita
     * @return \Illuminate\Http\Response
     */
    public function destroy($_id)
    {
        $berita = Berita::findOrFail($_id);
        $berita->delete();
        return redirect('kelola');
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $beritas = Berita::where('judul', 'like', "%{$search}%")->orWhere('nama_media', 'like', "%{$search}%")->get();
    
        return view('kelola', compact('beritas'));
    }

    public function filter(Request $request)
    {
        $date_from = $request->get('from_date');
        $date_to = $request->get('to_date');
        $state = $request->get('status');
        if($state != ""){
            $beritas = Berita::whereBetween('tanggal_terbit', [$date_from, $date_to])->where('status', $state)->get();
        }else{
            $beritas = Berita::whereBetween('tanggal_terbit', [$date_from, $date_to])->get();
        }
        
        //tambah sama by kategori

        return view('kelola', compact('beritas'));
    }

    public function myformAjax($id)
    {
        $cities = DB::connection('mongodb')
                    ->table("demo_cities")
                    ->where("state_id",$id)
                    ->lists("name","id");
        return json_encode($cities);
    }
}
